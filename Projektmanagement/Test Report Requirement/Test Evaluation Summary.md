# Test plan

## 1.	Introduction
### 1.1	Purpose
The purpose of the Iteration Test Plan is to gather all of the information necessary to plan and control the test effort for a given iteration. 
It describes the approach to testing the software.
This Test Plan for **PartyPlayer** supports the following objectives:
-	Identifies the items that should be targeted by the tests.
-	Identifies the motivation for and ideas behind the test areas to be covered.
-	Outlines the testing approach that will be used.
-	Identifies the required resources and provides an estimate of the test efforts.

### 1.2	Scope
This document describes the used tests, as they are unit tests and functionality testing.

### 1.3	Definitions, Acronyms, and Abbreviations
This document is meant for internal use primarily.

### 1.4	 References
| Reference        | 
| ------------- |
| [SAD](./sad.md) | 
| [UseCases](../UseCases) |

### 1.5 Overview
            
## 2.   Test Results Summary 

## 3.	Requirements-based Test Coverage
The listing below identifies those test items (software, hardware, and supporting product elements) that have been identified as targets for testing. 
This list represents what items will be tested. 

Items for Testing:
- SpringBoot-Web Backend
- Angular Frontend

## 4.	Code-based Test Coverage

## 5.	Suggested Actions

## 6.	Diagrams