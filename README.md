# TowerDefense

It was the year 2053, five years after the great catastrophe. The world as we knew it was no longer the same. A mysterious plague had nearly wiped out humanity, and the few survivors struggled daily to survive. Nature had reclaimed its place on Earth and with it came new, strange developments.

In a remote region that had once been a thriving village, something strange had occurred. The pigs, once peaceful farm animals of man, had made a new place for themselves in this new, wild world. They had become more intelligent and had begun to work together in large hordes to form their own society.

But they were not the only ones who had changed. The mushrooms that thrived all over the devastated land had undergone a remarkable metamorphosis due to the radiation and the changed soil. They were now huge and possessed peculiar abilities that made them extremely dangerous in the new world. They were able to release their spores into the air, which could then act as a dangerous weapon against other living things.

The pigs and the mushrooms lived peacefully side by side at first, but they soon discovered that they were both fighting for the same resources - food and space to live. Tensions rose between them, and eventually a bitter war broke out between the two factions.

The battles were relentless, and the pigs had to rely on their strength, speed and newly acquired intelligence to counter the attacking mushrooms. The mushrooms, on the other hand, used their size and deadly spores as weapons to keep the pigs at bay.

Over time, both sides developed more sophisticated tactics and strategies. The pigs formed elite units that were sent behind enemy lines to destroy the giant mushrooms from within. The mushrooms, in turn, began strategically placing their spores to lure the pigs into deadly traps. And thus a long era of battles between the pigs and mushrooms engulfed the world...

## Installation 

To provide the game to all of our fans, we plan to add it to the Play Store. You should be able to download and install the app directly from it.
Then you can simply run the game on your mobile devices.

![ezgif com-gif-maker](https://user-images.githubusercontent.com/64361270/209571899-32f89418-7b95-40ad-a992-f18f8abda449.gif)
